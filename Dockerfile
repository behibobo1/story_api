FROM node:14

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install

COPY . .

EXPOSE 3003
RUN npx sequelize-cli db:migrate --env development

CMD ["node", "app.js"]