'use strict';

const { faker } = require('@faker-js/faker');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    // Delete all existing records
    await queryInterface.bulkDelete('Images', null, {});
    await queryInterface.bulkDelete('Posts', null, {});
    await queryInterface.bulkDelete('Users', null, {});

    // Array to store all users, posts, and images
    const users = [];
    const posts = [];
    const images = [];

    // Generate 200 users
    for (let i = 1; i <= 20; i++) {
      users.push({
        username: faker.internet.userName(),
        email: faker.internet.email(),
        password: 'password',
        createdAt: new Date(),
        updatedAt: new Date()
      });
    }

    // Bulk insert users
    await queryInterface.bulkInsert('Users', users, {});

    // Fetch the newly inserted users
    const createdUsers = await queryInterface.sequelize.query(
      `SELECT id FROM "Users"`,
      { type: queryInterface.sequelize.QueryTypes.SELECT }
    );

    // Generate posts and images for each user
    for (const user of createdUsers) {
      for (let j = 1; j <= 1000; j++) {
        posts.push({
          title: faker.lorem.sentence(),
          description: faker.lorem.sentence(),
          user_id: user.id,
          event_date: faker.date.future(),
          createdAt: new Date(),
          updatedAt: new Date()
        });
      }
    }

    // Bulk insert posts and get the inserted posts
    await queryInterface.bulkInsert('Posts', posts, {});

    // Fetch the newly inserted posts
    const createdPosts = await queryInterface.sequelize.query(
      `SELECT id FROM "Posts"`,
      { type: queryInterface.sequelize.QueryTypes.SELECT }
    );

    // Generate images for each post
    for (const post of createdPosts) {
      for (let k = 1; k <= 2; k++) {
        images.push({
          url: faker.image.url(),
          post_id: post.id,
          createdAt: new Date(),
          updatedAt: new Date()
        });
      }
    }

    // Bulk insert images
    await queryInterface.bulkInsert('Images', images, {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Images', null, {});
    await queryInterface.bulkDelete('Posts', null, {});
    await queryInterface.bulkDelete('Users', null, {});
  }
};