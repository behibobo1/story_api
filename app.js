const express = require('express');
const authRoutes = require('./routes/auth');
const postRoutes = require('./routes/posts');
const userRoutes = require('./routes/users');
const path = require('path');
var cors = require('cors')
const app = express();
app.use(cors())

app.use('/uploads', express.static(path.join(__dirname, 'uploads')));

app.use(express.json());
app.use('/api/auth', authRoutes);
app.use('/api/posts', postRoutes);
app.use('/api/users', userRoutes);

app.listen(3003, () => {
  console.log('Server is running on port 3003');
});

module.exports = app;
