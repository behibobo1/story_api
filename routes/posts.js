const express = require('express');
const { Post, Image } = require('../models');
const auth = require('../middleware/auth');
const multer = require('multer');
const router = express.Router();
const path = require('path');

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'uploads/'); // Specify the destination directory
  },
  filename: function (req, file, cb) {
    const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9);
    const ext = path.extname(file.originalname);
    cb(null, file.fieldname + '-' + uniqueSuffix + ext);
  }
});

const upload = multer({ storage: storage });

router.post('/', auth, async (req, res) => {
  const { title, description } = req.body;
  const post = await Post.create({ title, description, user_id: req.user.id });
  res.status(201).send(post);
});

router.get('/', auth, async (req, res) => {
  try {
    // Retrieve posts belonging to the logged-in user
    const posts = await Post.findAll({
      where: { user_id: req.user.id },
      include: [
        {
          model: Image,
          as: 'images',
        },
      ],
    });

    res.status(200).json(posts);
  } catch (error) {
    console.error('Error fetching user posts:', error);
    res.status(500).send('Internal Server Error');
  }
});

router.post('/:post_id/images', auth, upload.array('images'), async (req, res) => {
  const { post_id } = req.params;
  const post = await Post.findByPk(post_id);
  if (!post || post.user_id !== req.user.id) {
    return res.status(403).send('Access denied');
  }

  const images = req.files.map(file => ({ url: file.path, post_id: post_id }));
  await Image.bulkCreate(images);
  res.status(201).send(images);
});

module.exports = router;
