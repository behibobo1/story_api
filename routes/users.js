const express = require('express');
const { User, Post, Image } = require('../models');
const auth = require('../middleware/auth');
const multer = require('multer');
const router = express.Router();
const path = require('path');

router.get('/', auth, async (req, res) => {
  try {
    // Retrieve pagination parameters from the query string
    const { page = 1, pageSize = 10 } = req.query;

    // Calculate offset based on current page and page size
    const limit = parseInt(pageSize);
    const offset = (parseInt(page) - 1) * limit;

    // Retrieve users with pagination and include their posts
    const users = await User.findAll({
      include: [
        {
          model: Post,
          as: 'posts',
        },
      ],
      limit,
      offset,
    });

    res.status(200).json(users);
  } catch (error) {
    console.error('Error fetching user posts:', error);
    res.status(500).send('Internal Server Error');
  }
});

module.exports = router;
