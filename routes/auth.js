const express = require('express');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { User } = require('../models');
const router = express.Router();

router.post('/signup', async (req, res) => {
  const { username, email, password } = req.body;
  if (!username || !email || !password) {
    return res.status(400).json({ message:'All fields are required' });
  }
  const usernameExists = await User.findOne({ where: { username } });
  if (usernameExists) {
    return res.status(400).json({ message:'Username already exists' });
  }

  const emailExists = await User.findOne({ where: { email } });
  if (emailExists) {
    return res.status(400).json({ message:'Email already exists' });
  }

  const hashedPassword = await bcrypt.hash(password, 10);
  const user = await User.create({ username, email, password: hashedPassword });
  res.status(201).json(user);
});

router.post('/signin', async (req, res) => {
  const { email, password } = req.body;
  if (!email || !password) {
    return res.status(400).json({ message: 'All fields are required' });
  }
  const user = await User.findOne({ where: { email } });
  if (!user) {
    return res.status(400).json({ message: 'Invalid email or password' });
  }
  const isMatch = await bcrypt.compare(password, user.password);
  if (!isMatch) {
    return res.status(400).json({ message: 'Invalid email or password' });
  }
  const token = jwt.sign({ id: user.id }, 'your_jwt_secret', { expiresIn: '10h' });
  res.status(200).json({ token });
});

module.exports = router;
